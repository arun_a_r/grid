diffusion_timer = 0
function init()
  state = "ready"
  prev_state = "dummy"
  diffuse_state = "contact"
  prev_diffuse_state = "dummy"
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  robot.colored_blob_omnidirectional_camera.enable()
end
--------------------------------------------------------------------------------
function step()
  if state ~= prev_state then
    log(self_addr,"=",state)
  end
  prev_state = state

  if state == "ready" then
    ready()
  elseif state == "responding_to_broadcast" then
    responding_to_broadcast()
  elseif state == "waiting_for_final_call" then
    waiting_for_final_call()
  elseif state == "go" then
    go()
  elseif state == "diffuse" then
    diffuse()
  elseif state == "return" then
    remove()
  end
end
--------------------------------------------------------------------------------
function reset()
end
--------------------------------------------------------------------------------
function destroy()
end
--------------------------------------------------------------------------------
-----------------------------function addr()------------------------------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------function compose()---------------------------------
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
end
--------------------------------------------------------------------------------
-----------------------------function ready()-----------------------------------
--------------------------------------------------------------------------------
function ready()
  caller = 0
  if #robot.range_and_bearing > 0 then
    for i =1,#robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == 255 then
        log("received broadcast")
        caller = robot.range_and_bearing[i]
      end
    end
  end

  if caller ~= 0 then
    state = "responding_to_broadcast"
  end
end
--------------------------------------------------------------------------------
function responding_to_broadcast()
  send_ack = compose(caller.data[1],2)
  robot.range_and_bearing.set_data(send_ack)
  state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
function waiting_for_final_call()
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr
          and robot.range_and_bearing[i].data[3] == 3 then

        state = "go"
      elseif robot.range_and_bearing[i].data[2] ~= self_addr
              and robot.range_and_bearing[i].data[3] == 3 then

        state = "ready"
      end
    end
  end
end
--------------------------------------------------------------------------------
function go()
  sensingLeft =     robot.proximity[3].value +
                    robot.proximity[4].value +
                    robot.proximity[5].value +
                    robot.proximity[6].value +
                    robot.proximity[2].value +
                    robot.proximity[1].value

  sensingRight =    robot.proximity[19].value +
                    robot.proximity[20].value +
                    robot.proximity[21].value +
                    robot.proximity[22].value +
                    robot.proximity[24].value +
                    robot.proximity[23].value

  if #robot.range_and_bearing then

    for i =1, #robot.range_and_bearing do

      if robot.range_and_bearing[i].data[2] == self_addr and
          robot.range_and_bearing[i].data[3] == 3 then

        if robot.range_and_bearing[i].range > 50 then

          if sensingLeft ~= 0 then
            robot.wheels.set_velocity(7,3)
          elseif sensingRight ~= 0 then
            robot.wheels.set_velocity(3,7)
          elseif robot.range_and_bearing[i].horizontal_bearing > 0 and
            robot.range_and_bearing[i].horizontal_bearing <= 0.5 then

             robot.wheels.set_velocity(9,10)

          elseif robot.range_and_bearing[i].horizontal_bearing >0.5 then

                  robot.wheels.set_velocity(3,7)

          elseif robot.range_and_bearing[i].horizontal_bearing < 0 and
                  robot.range_and_bearing[i].horizontal_bearing >= -0.5 then

                  robot.wheels.set_velocity(10,9)

          elseif robot.range_and_bearing[i].horizontal_bearing < -0.5 then
                        robot.wheels.set_velocity(7,3)
          end

        elseif robot.range_and_bearing[i].range < 50 then

          if #robot.colored_blob_omnidirectional_camera > 0 then
            dist = 100
            this_ang = robot.colored_blob_omnidirectional_camera[1].angle
            for i = 1, #robot.colored_blob_omnidirectional_camera do

              if robot.colored_blob_omnidirectional_camera[i].color.red == 255 and
                  robot.colored_blob_omnidirectional_camera[i].distance < dist then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                this_ang = robot.colored_blob_omnidirectional_camera[i].angle

              end
            end

            if dist > 30 and this_ang > 0 then
                robot.wheels.set_velocity(3,7)
            elseif dist > 30 and this_ang < 0 then
                robot.wheels.set_velocity(7,3)
            elseif dist < 30 and dist > 29 then
                robot.wheels.set_velocity(5 * (dist - 1), 5 * (dist - 1))
            elseif dist < 29 and dist > .1 then
                state = "diffuse"
            end
          end
        end
      end
    end
  end
end
--------------------------------------------------------------------------------
function diffuse()
  robot.wheels.set_velocity(0,0)
  if diffuse_state ~= prev_diffuse_state then
    log(self_addr,"=",diffuse_state)
  end
  prev_diffuse_state = diffuse_state
  if diffuse_state == "contact" then
    contact()
  end
end
--------------------------------------------------------------------------------
----------------------internal diffuse states-----------------------------------
--------------------------------------------------------------------------------
function contact()
  cont_mine = compose(255,4)
  robot.range_and_bearing.set_data(cont_mine)
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr and
          robot.range_and_bearing[i].data[3] == 5 then

          mine = robot.range_and_bearing[i]
          diffuse_state = "processing"
      end
    end
  end
end
--------------------------------------------------------------------------------
function processing()
  diffusion_timer = diffusion_timer + 1
  cont_mine = compose(mine.data[1],6)
  if diffusion_timer >= 50 then
    robot.range_and_bearing.set_data(cont_mine)
  end
  diffusion_timer = 0
  diffuse_state = "contact"
  state = "return"
end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function remove()
  robot.leds.set_single_color(13,"green")

end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
