msg = {}
msg[1] = "Found Mine"
msg[2] = "ready to help"
msg[3] = "ack from initiator / beacon"
msg[4] = "contact mine"
msg[5] = "mine connected"
msg[6] = "mine disarmed"
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function init()
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  state = "armed"
  prev_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end
--------------------------------------------------------------------------------
function step()
  if state ~= prev_state then
      log(self_addr,"=",state)
  end
  prev_state = state
  if state == "armed" then
    armed()
  elseif state == "precarious" then
    precarious()
  elseif state == "disarmed" then
    disarmed()
  end
end
--------------------------------------------------------------------------------
function reset()
end

function destroy()
end
--------------------------------------------------------------------------------
-----------------------------function addr()------------------------------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------function compose()---------------------------------
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
end
--------------------------------------------------------------------------------
function armed()
  robot.leds.set_single_color(13,"red")
  if #robot.range_and_bearing > 0 then
    for i =1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == 255 and
          robot.range_and_bearing[i].data[3] == 4 and
          robot.range_and_bearing[i].range <30 then

        diffuser = robot.range_and_bearing[i]
        mine_stat = compose(diffuser.data[1],5)
        robot.range_and_bearing.set_data(mine_stat)
        state = "precarious"
      end
    end
  end
end
--------------------------------------------------------------------------------
function precarious()
  robot.leds.set_all_colors("red")
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr and
          robot.range_and_bearing[i].data[3] == 6 then

        state = "disarmed"
      end
    end
  end
end
--------------------------------------------------------------------------------
function disarmed()
  robot.leds.set_all_colors("black")
end
--------------------------------------------------------------------------------
