--------------------------------------------------------------------------------
------messages
msg = {}
msg[1] = "Found Mine"
msg[2] = "ready to help"
msg[3] = "ack from initiator / beacon"
msg[4] = "try diffuse to mine"
--------------------------------------------------------------------------------
function init()
  self_addr = addr(robot.id)
  log(robot.id," = ",id)
  state = "search"
  prev_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end

function step()
  if state ~= prev_state then
      log(self_addr,"=",state)
  end
  prev_state = state
  if state == "search" then
    search()
  elseif state == "call" then
    call()
  elseif state == "specific_call" then
    specific_call()
  end
end
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
-----------------------------function addr()------------------------------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------function compose()---------------------------------
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
 -------------------------------------------------------------------------------
function search()
  sign = robot.positioning.orientation.axis.z
  ang = robot.positioning.orientation.angle
  robot.wheels.set_velocity(5,5)

  sensingLeft =     robot.proximity[3].value +
                    robot.proximity[4].value +
                    robot.proximity[5].value +
                    robot.proximity[6].value +
                    robot.proximity[2].value +
                    robot.proximity[1].value

  sensingRight =    robot.proximity[19].value +
                    robot.proximity[20].value +
                    robot.proximity[21].value +
                    robot.proximity[22].value +
                    robot.proximity[24].value +
                    robot.proximity[23].value
  if sensingLeft ~= 0 then
    robot.wheels.set_velocity(0,0)
    state = "call"
  end
end
--------------------------------------------------------------------------------
function call()
  robot.wheels.set_velocity(0,0)
  broadcast_msg = compose(255,1)
  robot.range_and_bearing.set_data(broadcast_msg)
  log("sending broadcast")
  distance = 10000
  nearest = 0
  if #robot.range_and_bearing > 0 then
    for i = 1, #robot.range_and_bearing do
      if (robot.range_and_bearing[i].data[2] == self_addr and
         robot.range_and_bearing[i].data[3] == 2 and
         robot.range_and_bearing[i].range < distance) then

           distance = robot.range_and_bearing[i].range
           nearest = robot.range_and_bearing[i]
      end
    end
  end
  if nearest ~= 0 then
    state = "specific_call"
  end
end
--------------------------------------------------------------------------------
function specific_call()
  specific_msg = compose(nearest.data[1],3)
  robot.range_and_bearing.set_data(specific_msg)
  if #robot.range_and_bearing > 0 then
    for i = 1,#robot.range_and_bearing do
      if robot.range_and_bearing[i].data[2] == self_addr and
          robot.range_and_bearing[i].data[3] == 7 then

            state = "search"
      end
    end
  end
end
